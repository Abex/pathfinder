package pathfinder_test

import (
	"fmt"
	"os"
	"path/filepath"

	"bitbucket.org/abex/pathfinder"
)

func ExampleFindDir() {
	path, err := pathfinder.FindDir("bitbucket.org/abex/pathfinder", ".")
	if err != nil {
		panic(err)
	}
	if _, err := os.Stat(filepath.Join(path, "pathfinder.go")); err == nil {
		fmt.Println("ok!")
	}
	// Output: ok!
}
