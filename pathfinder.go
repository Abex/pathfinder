//Copyright (c) 2017, Abex
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the FreeBSD Project.

// package pathfinder provides a simple api for finding a directory located with your source folder
package pathfinder

import (
	"fmt"
	"os"
	"path/filepath"
)

func trypath(dirname string) (string, error, bool) {
	stat, err := os.Stat(dirname)
	if err == nil && stat.IsDir() {
		return dirname, nil, true
	}
	if !os.IsNotExist(err) {
		return "", err, true
	}
	return "", nil, false
}

// CannotFindError is returned when FindDir fails to find the named directory
type CannotFindError struct {
	dir string
}

func (c CannotFindError) Error() string {
	return fmt.Sprintf("pathfinder: Cannot find directory '%v'", c.dir)
}

func (c CannotFindError) String() string {
	return c.Error()
}

// FindDir looks for Directory 'dirname' in the working directory,
// then in pkgname's source directory. It works with multiple GOPATHs
func FindDir(pkgname, dirname string) (ret string, err error) {
	var ok bool
	ret, err, ok = trypath(dirname)
	if ok {
		return
	}
	gopaths := filepath.SplitList(os.Getenv("gopath"))
	for _, path := range gopaths {
		ret, err, ok = trypath(filepath.Join(path, "src", pkgname, dirname))
		if ok {
			return
		}
	}
	return "", CannotFindError{dirname}
}
