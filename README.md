Pathfinder
==========

[![GoDoc](https://godoc.org/bitbucket.org/abex/pathfinder?status.svg)](https://godoc.org/bitbucket.org/abex/pathfinder)

Pathfinder is a small library to find where your go code is located, for finding assets or whatnot.

		go get bitbucket.org/abex/pathfinder

 

		import "bitbucket.org/abex/pathfinder"

Example
-------

	package main

	import (
		"net/http"
		"bitbucket.org/abex/pathfinder"
	)

	func main(){
		www, err := pathfinder.FindDir("bitbucket.org/abex/test", "www")
		if err != nil {
			panic(err)
		}
		http.Handle("/",http.FileServer(http.Dir(www)))
		panic(http.ListenAndServe(":80",nil))
	}